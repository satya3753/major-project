FROM python:3.8

ADD real-time-mask-detction.ipynb,face-mask-image-classification-with-keras.h5,haarcascade_frontalface_default.xml

RUN pip install -r requirement.txt

CMD [""]